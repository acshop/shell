#!/bin/bash
#日志路径
#log_files_path=/weblogic/cpslogs/
log_files_path=/weblogic/cpslogs/
#weblogic日志备份路径
log_files_backpath=/weblogic/cpslogs/logback/server/
#应用日志备份路径
log_files_appbackpath=/weblogic/cpslogs/logback/app/
#应用日志路径
log_app_path=/home/weblogic/user_projects/logs/

d=`date +%Y-%m-%d`

#日志以年/月的目录形式存放
log_files_dir=${log_files_path}$(date -d "yesterday" +"%Y")

#设置需要进行日志分割的日志文件名称，以空格隔开
log_files_name=(admin.out server1.out server2.out person.out ent.out)
############################################
#Please do not modify the following script #
############################################
mkdir -p $log_files_dir

log_files_num=${#log_files_name[@]}
 
#cut log_files_name log files
echo "准备切割日志！"
for((i=0;i<$log_files_num;i++));do
cp ${log_files_path}${log_files_name[i]} ${log_files_dir}/${log_files_name[i]}_$(date -d "yesterday" +"%Y%m%d")
echo "" > $log_files_path/${log_files_name[i]}
done
echo "切割日志任务完成！"

#设置时间节点
name1=`date -d"7days ago" +%Y%m%d`;
name2=`date -d"14days ago" +%Y%m%d`;

echo "准备执行压缩7天前日志并删除源日志，移动压缩日志到logback的任务！"

#压缩7天前日志（根据实际日志名称设置，weblogic控制台/server服务/结构化共5个日志文件）
cd $log_files_dir/
zip -r log_back_$name1.zip . -i ./*$name1;

#删除7天前未压缩文件
rm -rf *$name2;
#根据需要是否要移动压缩的日子
mv *.zip /weblogic/cpslogs/logback/;
echo "任务完成！"


#删除14天前weblogic备份的压缩日志
echo "准备执行删除14天前压缩日志任务！"
cd $log_files_backpath
rm -rf *$name2.zip;

#清理应用日志
echo "准备执行压缩7天前日志并删除源日志，移动压缩日志到logback的任务！"
app_dir=$log_app_path/*/*$name1.log
target_dir=$log_files_appbackpath
filelist=`ls $app_dir`
#遍历查找并移动同一天的应用日志
for file in $filelist
do
res_file=`echo $file | awk -F "/" '{print $(NF-1)}'`
echo $res_file
echo $file
mv $file $target_dir$res_file".$name1.log"
done
#处理备份日志
cd $log_files_appbackpath
zip -r log_back_$name1.zip . -i ./*$name1.log;
rm -rf *$name1.log;
rm -rf $name2.zip;

echo "任务完成！"
echo "日志清理策略任务完成！"
