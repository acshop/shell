#!/bin/bash
#description: Nginx install script.
# by oilive
soft_home=/home/soft/nginx/
nginx_home=/home/weblogic/nginx/
logs_name=${soft_home}install_nginx.log
#
#安装环境依赖包：
#1、 gcc 语言编译器套件。
#2、 pcre 兼容正则表达式的库 rewrite 模块需要。
#3、 zlib 提供数据压缩函数库 例如gzip压缩。
#4、 openssl 使用https所需的ssl。
#yum -y install gcc zlib zlib-devel pcre-devel openssl openssl-devel
#判断是否已经安装（不权威）
judge(){
	rpm -q $1 &>/dev/null
}
#安装
install(){
	soft=${1%%-*}  #删除最后（最左边）一个 - 号及右边的字符
	if judge ${soft}-devel; then
		echo "${soft}-devel ready to install."
		echo y | yum -y install ${soft}-devel
	fi
	if judge $soft; then
		echo '${soft} ready to install.'
		cd ${soft_home}$1/
		./configure && make && make install
		[ $(echo $?) -eq 0 ] && echo "$1 install success!!!"
	else
		echo "$soft is already installed."
	fi
}
#安装nginx-1.8.1
nginx(){
	[ -f ${nginx_home}conf/nginx.conf ] && return 1 #如果已经安装，不执行
	[ -d $nginx_home ] || mkdir $nginx_home
	cd ${soft_home}${1}/
	./configure --prefix=${nginx_home%?}
	[ $(echo $?) -eq 0 ] && make && make install
	[ $(echo $?) -eq 0 ] && echo "nginx install success!!!"
}
#解压并安装
unzipAndInstall(){
	wjs=(`find ${soft_home} -type f -name *.tar.gz`)
	if [ ${#wjs[@]} -ne 0 ]; then
		for((i=0;i<${#wjs[@]};i++));do
			cd ${soft_home}
			soft=${wjs[i]##*/}  #删除最后（最右边）一个 / 号及左边的所有字符
			tar -zxvf $soft && rm $soft
			echo "Extracting wav from $soft"
			if [ "${soft%%-*}" = "nginx" ] ; then
				nginx ${soft%%.tar*}
			else
				install ${soft%%.tar*}
			fi
		done
	else
		echo "No have *.tar.gz file!"
	fi
}
main(){
	unzipAndInstall
	#source service nginx restart
}
main
#> ${logs_name}
#tail -100f ${logs_name}
