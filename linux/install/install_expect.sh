#!/bin/sh
#fir=/home/soft/
#[ -d $fir ] || mkdir $fir

dir=/home/soft/expect/
[ -d $dir ] || mkdir $dir

#./home/copy.sh $dir expect-5.43.0.tar.gz
#./home/copy.sh $dir tcl8.4.11-src.tar.gz

cd $dir
tar -zxvf expect-5.43.0.tar.gz
tar -zxvf tcl8.4.11-src.tar.gz

cd tcl8.4.11/unix/
./configure
make && make install

cd ${dir}expect-5.43/
./configure --with-tclinclude=${dir}tcl8.4.11/generic --with-tclconfig=/usr/local/lib/
make && make install
[ $(echo $?) -eq 0 ] && echo 'expect安装成功'
