#!/bin/bash
#decription: Rabbitmq install script.
# by oilive
soft_home=/home/soft/rabbitmq/
rabbitmq_home=/home/weblogic/rabbitmq/
#集群主机名称&ip
app_name=app07
#节点地址&root用户密码
cluster_host=192.168.43.176
host_pwd=root123
#当前服务器名称&ip
name=`cat /etc/hostname`
host=`hostname -I`

#判断是否已经安装（不权威）
judge(){
	rpm -q $1 &>/dev/null
}
ncurses(){
	if judge ncurses; then
		echo 'ncurses is already installed.'
	else
		echo 'ncurses ready to install.'
		cd $soft_home
		tar -zxvf ncurses.tar.gz
		cd ncurses-6.2/
		./configure --with-shared --without-debug --without-ada --enable-overwrite
		make && make install
	fi
	if judge ncurses-devel; then
		echo 'ncurses-devel is already installed.'
	else
		echo 'ncurses-devel ready to install.'
		echo y | yum install ncurses-devel
	fi
}
openssl(){
	if judge openssl; then
		echo 'openssl is already installed.'
	else
		echo 'openssl ready to install.'
		cd $soft_home
		tar -zxvf openssl-1.0.1s.tar.gz
		cd openssl-1.0.1s/
		./config --prefix=/usr/local/openssl
		last_var="CFLAG= -DOPENSSL_THREADS"
		new_var="CFLAG= -fPIC -DOPENSSL_THREADS"
		sed -i "s/$last_var/$new_var/g" Makefile
		[ $(echo $?) -eq 0 ] && make && make install
	fi
	if judge openssl-devel; then
		echo 'openssl-devel is already installed.'
	else
		echo 'openssl-devel ready to install.'
		echo y | yum install openssl-devel
	fi
}
findSsl(){
	ssh=`find / -name ssl.h`
	[ -f $ssh ] && echo ${ssh%/*}
}
erlang(){
	if judge gcc; then
		echo y | yum -y install gcc gcc-c++ autoconf make automake
	fi
	cd $soft_home
	tar -zxvf otp_src_19.1.tar.gz
	cd otp_src_19.1/
	ssl="--with-ssl=/usr/local/openssl"
	judge openssl && ssl="--with-ssl=/usr/include/openssl"
	[ -z findSsl ] && ssl= findSsl
	./configure --prefix=${rabbitmq_home}erlang --without-javac $ssl
	#--enable-threads --enable-smp-support --enable-kernel-poll --enable-hipe --without-javac
	make && make install
	ebin=${rabbitmq_home}erlang/bin
	grep $ebin /etc/profile
	[ $(echo $?) -ne 0 ] && echo "export PATH=\$PATH:$ebin" >>/etc/profile
	#sed -i '$a export PATH=$PATH: /home/rabbitmq/erlang/bin' /etc/profile
	echo "手动输入：source /etc/profile 后，再执行./install_rabbitmq.sh rabbitmq"
}
erlang(){
	if judge gcc; then
		echo y | yum -y install gcc gcc-c++
	fi
	cd $soft_home
	tar -zxvf otp_src_19.1.tar.gz
	cd otp_src_19.1/
	ssl="--with-ssl=/usr/local/openssl"
	judge openssl && judge openssl-devel && ssl="--with-ssl=/usr/include/openssl"
	#[ -z findSsl ] && ssl= findSsl
    ./configure --prefix=${rabbitmq_home}erlang --without-javac $ssl
	make && make install
	if [ $(echo $?) -eq 0 ] ; then
		ebin=${rabbitmq_home}erlang/bin
		grep $ebin /etc/profile
		[ $(echo $?) -ne 0 ] && echo "export PATH=\$PATH:$ebin" >>/etc/profile
		echo "手动输入：source /etc/profile 后，再执行./install_rabbitmq.sh rabbitmq"
	else 
		echo "未安装成功，请查看日志!"
	fi
}
#启用rabbitmq、启用插件，创建用户
rabbitmq(){
	cd $soft_home
	tar -zxvf rabbitmq_server-3.6.10.tar.gz
	mv rabbitmq_server-3.6.10/* ${rabbitmq_home}
	chmod +x ${rabbitmq_home}sbin/*
	cd ${rabbitmq_home}sbin/
	./rabbitmq-server -detached
	./rabbitmq-plugins enable rabbitmq_management
	./rabbitmqctl add_user root root
	./rabbitmqctl set_user_tags root administrator
	./rabbitmqctl set_permissions -p "/" root ".*" ".*" ".*"
	systemctl status firewalld | grep 'running'
	[ $(echo $?) -eq 0 ] && systemctl stop firewalld
	#暂时关闭防火墙 systemctl stop firewalld
	#永久关闭防火墙 systemctl disable firewalld
}
#获取其他服务器ip
getIp(){
	file=/home/hosts
/usr/local/bin/expect <<EOF
spawn ssh root@${cluster_host}
set timeout 30
expect {
  -re "password" {send "$host_pwd\n"} 
  -re "yes/no" {send "yes\n";exp_continue} # 有的时候输入几次密码来确认,exp_continue
}
expect "#"
send "rm -f $file\r"
expect "#"
send "hostname -I > ${file}\r"
expect "#"
send "cat /etc/hostname >> ${file}\r"
expect "#"
send "exit\r"
expect eof
EOF
[ -f $file ] && rm -rf $file
/usr/local/bin/expect <<EOF
set timeout 20
spawn scp -r -p root@${cluster_host}:$file $file
expect {
 "(yes/no)?"
 {
   send "yes\n"
   expect "*assword:" {send "$host_pwd\n"}
 }
 "*assword:"
 {
   send "$host_pwd\n"
 }
}
set timeout 10
send "exit\n"
expect eof
EOF
[ -f $file ] && cat $file
}
#配置集群host目录
host(){
	grep $cluster_host /etc/hosts
	[ $(echo $?) -ne 0 ] && getIp && echo $(cat $file) >> /etc/hosts
	#[ $(echo $?) -ne 0 ] &&  echo $cluster_host$1 >> /etc/hosts
	grep $host /etc/hosts
	[ $(echo $?) -ne 0 ] && echo "$host$name" >> /etc/hosts
}
#同步集群服务器cookie
cluster_cookie(){
	cks=`find / -name .erlang.cookie`
	source ./scp.sh $cluster_host $cks
	echo "成功发送文件!!!"
}
#集群配置
cluster(){
	cluster_cookie
	cd ${rabbitmq_home}sbin/
	./rabbitmqctl stop_app 
	./rabbitmqctl join_cluster rabbit@$app_name
	./rabbitmqctl start_app
	#./rabbitmqctl cluster_status
	status
}
#查看集群状态
status(){
	echo "$name rabbitmq集群状态："
	cd ${rabbitmq_home}sbin/
	./rabbitmqctl cluster_status
}
case $1 in
install)
	#检查并安装ncurses、ncurses-devel
	ncurses
	#检查并安装openssl、openssl-devel
	openssl
	#检查gcc等组件是否安装，并安装erlang
	#并配置环境变量
	erlang
;;
rabbitmq)
	#安装rabbitmq并配置启动
	rabbitmq
;;
cluster)
	#配置集群并启动
	cluster
;;
host)
	host app07
;;
get)
	getIp
;;
judge)
	ssl="--with-ssl=/usr/local/openssl"
	#judge openssl && ssl="--with-ssl=/usr/include/openssl"
	[ -z findSsl ] && ssl= findSsl
	echo $ssl
;;
esac